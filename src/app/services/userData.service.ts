import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';
import { environment } from '../../enviroments/enviroment';


@Injectable({
  providedIn: 'root',
})
export class UserDataService {
  constructor(private http: HttpClient) {}

  getAllUser(): Observable<User[]> {
    return this.http.get<User[]>(environment.UrlBase);
  }

  getUser(id: any): Observable<User> {
    return this.http.get<User>(`${environment.UrlBase}/${id}`);
  }

  createUser(data: any): Observable<any> {
    return this.http.post(environment.UrlBase, data);
  }

  updateUser(id: any, data: any): Observable<any> {
    return this.http.put(`${environment.UrlBase}/${id}`, data);
  }

  deleteUser(id: any): Observable<any> {
    return this.http.delete(`${environment.UrlBase}/${id}`);
  }

}
