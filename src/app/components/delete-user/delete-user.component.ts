import { UserDataService } from './../../services/userData.service';

import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrl: './delete-user.component.css'
})

export class DeleteUserComponent {


  @Input() user:any;
  @Output() loadUser = new EventEmitter();




  
   constructor(private userDataService: UserDataService){
   }


   /**
    * Permite borrar a un usuario
    */
   deleteUser(){
    console.log(this.user);
    this.userDataService.deleteUser(this.user.id).subscribe(
      res => {
        console.log(res);
        this.loadUser.emit(true);
      },
      error => {
        this.loadUser.emit(false);
      }
      
    )
   }


}
