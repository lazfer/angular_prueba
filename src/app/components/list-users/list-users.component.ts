import { Component } from '@angular/core';
import { Router} from '@angular/router';
import { UserDataService } from '../../services/userData.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrl: './list-users.component.css'
})
export class ListUsersComponent {

  public listUser:any = [];
  public user = {};
constructor(public router: Router,
  private userData: UserDataService
){
  this.getUsers();
  }

  getUsers(){
    this.userData.getAllUser().subscribe( (data:any) => {
      console.log(data)
      this.listUser = data;
    })
  }

/**
 * Direcciona a la vista nuevo usuario
 */
  newUser(){
    this.router.navigate(['add-user']);
  }
/**
 * Borra al uusario
 * @param user 
 */
  deleteUser(user:User){
   this.user = user;
  }


  /**
   * Carga de nuevo la lista
   */
  loadUser(){
    this.getUsers();
  }

  editUser(user:User){
    this.router.navigate(['edit-user', user.id])
  }
}
