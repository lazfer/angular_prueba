import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {

  constructor(public router: Router) {

  }

  /**
   * Direcciona al home
   */
  home(){
    this.router.navigate(['/']);
  }

  /**
   * Direcciona a la vista para agregar un nuevo usuario
   */
  addUser(){
   this.router.navigate(['add-user']);
  }
}
