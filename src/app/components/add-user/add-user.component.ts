import { UserDataService } from './../../services/userData.service';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {v4 as uuidv4} from 'uuid';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrl: './add-user.component.css'
})
export class AddUserComponent{

   public objData = {};
  
  public formUser: FormGroup = this.formBuilder.group({
    name: ["", [Validators.required]],
    lastName: ["", [Validators.required]],
    age: ["", [Validators.required]],
    email: ["", [Validators.required, Validators.email]],
    description: [""]
  })

  constructor(public formBuilder: FormBuilder, private userDataService: UserDataService, private router: Router){
  }
  /**
   * Guarda los datos del usuario
   */
  save(){
    let id = uuidv4();

     this.objData = {
     id: id,
     name: this.formUser.get('name')?.value,
     lastName: this.formUser.get('lastName')?.value,
     age: this.formUser.get('age')?.value,
     email: this.formUser.get('email')?.value,
     description: this.formUser.get('description')?.value
    }
    this.userDataService.createUser(this.objData).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/']);
      }
    )

  }
}
