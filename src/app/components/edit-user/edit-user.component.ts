import { UserDataService } from './../../services/userData.service';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {v4 as uuidv4} from 'uuid';
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrl: './edit-user.component.css'
})
export class EditUserComponent {


  public objData = {};
  public id: any;
  
  public formUser: FormGroup = this.formBuilder.group({
    name: ["", [Validators.required]],
    lastName: ["", [Validators.required]],
    age: ["", [Validators.required]],
    email: ["", [Validators.required, Validators.email]],
    description: [""]
  })

  constructor(public formBuilder: FormBuilder, private userDataService: UserDataService, private route: ActivatedRoute, private router: Router){
    this.getIdUser();
  }

  /**
   * Recupera el id del usuario
   */

  getIdUser(){
    this.route.paramMap.subscribe( param => {
      let id = param.get('id');
      this.getUser(id);
    })
  }


  /**
   * Recupera al usuario
   * @param id 
   */

  getUser(id:any){
   this.userDataService.getUser(id).subscribe(
    res => {
      console.log(res);
      this.id = res.id;
      this.formUser.get('name')?.setValue(res.name);
      this.formUser.get('lastName')?.setValue(res.lastName);
      this.formUser.get('age')?.setValue(res.age);
      this.formUser.get('email')?.setValue(res.email);
      this.formUser.get('description')?.setValue(res.description);
    }
   )
  }

  /**
   * Guarda los datos del usuario
   */
  save(){
     this.objData = {
     name: this.formUser.get('name')?.value,
     lastName: this.formUser.get('lastName')?.value,
     age: this.formUser.get('age')?.value,
     email: this.formUser.get('email')?.value,
     description: this.formUser.get('description')?.value
    }
    this.userDataService.updateUser(this.id, this.objData).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/']);
      }
    )

  }

}
