# Instalacción de dependencias

Una vez clonado el proyecto lo deberas abrir en una terminal y ejecutar el siguiente comando:

npm install

# Iniciar proyecto

Una vez instaladas las dependencias debemos iniciar el servidor con el siguiente comando:

ng serve

# Iniciar json server

Para iniciar el servidor de datos dummie debemos ejecutar el siguiente comando en la terminal del proyecto

npx json-server --watch db.json
